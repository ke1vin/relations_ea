import com.kelvingu.relations.graph.Edge;
import com.kelvingu.relations.utils.FeatureStats;
import com.kelvingu.relations.utils.IO;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ErrorAnalysis {

    public static String errorDir = "data/nell";
    //public static String errorDir = "data/freebase";

    // some dataset dependent parameters

    // NELL
    public static double pseudoCount = 3;
    public static double weightThreshold = 0.6862745;

    // Freebase
    //public static double pseudoCount = 3;
    //public static double weightThreshold = 0.8444444;

    public static FeatureStats fs;
    public static Map<String, FeatureStats> fsByType;
    public static List<Edge> wrongEdges;

    /**
     * Run this in debug mode.
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException {
        // load featureStats
        File fsFile = new File(errorDir, "fsByType.ser");
        fsByType = IO.<Map<String, FeatureStats>>load(fsFile);
        System.out.println("Loaded featureStats");

        // form fs from fsByType
        fs = new FeatureStats();
        for (FeatureStats fst : fsByType.values()) {
            fst.forEach((feat, stat) -> fs.put(feat, stat));
        }

        // load wrongEdges
        File edgeFile = new File(errorDir, "wrongEdges.ser");
        wrongEdges = IO.<List<Edge>>load(edgeFile);

        // Put a breakpoint here
        System.out.println("Done.");
    }

    public static List<FeatureStats.MultiStat> look(int exNum) {
        Edge edge = wrongEdges.get(exNum);
        return fs.get(edge).getStatsByTrainProbExtremity(4, -1);
    }

    public static List<FeatureStats.MultiStat> getStatsByNumWrong(int topK) {
        List<FeatureStats.MultiStat> list = fs.getStatsBy(ErrorAnalysis::getNumWrong, topK);
        double totalWrong = list.stream().mapToDouble(ErrorAnalysis::getNumWrong).sum();
        System.out.println(totalWrong);
        return list;
    }

    public static double getNumWrong(FeatureStats.MultiStat ms) {
        double p = ms.train.getProb(pseudoCount);
        int[][] cfm = ms.testPivot.confusionMatrix;
        double neg = cfm[1][0];
        double pos = cfm[1][1];
        double numWrong = p >= weightThreshold ? neg : pos;
        return numWrong;
    }

    /**
     * this assumes that if the feature's precision was >= weightThreshold, the classifier labeled true, otherwise false
     */
    public static List<FeatureStats.MultiStat> getStatsByPercentWrong(int topK, int pivotPrecisionPseudoCount) {
        FeatureStats.AttribSelector selector = (ms) -> {
            int[][] cfm = ms.testPivot.confusionMatrix;
            double neg = cfm[1][0];
            double pos = cfm[1][1];
            double total = neg + pos + (2 * pivotPrecisionPseudoCount);

            // if it was never the pivot, we consider it as having done no wrong
            if (total == 0) {
                return 0d;
            }

            double numWrong = getNumWrong(ms) + pivotPrecisionPseudoCount;
            return numWrong / total;
        };
        List<FeatureStats.MultiStat> multiStats = fs.getStatsBy(selector, -1);

        // filter out all multiStats that have never been the pivot
        Predicate<FeatureStats.MultiStat> wasPivot = ms -> {
            int[][] cfm = ms.testPivot.confusionMatrix;
            int total = cfm[1][0] + cfm[1][1];
            return total != 0;
        };

        List<FeatureStats.MultiStat> filtered = multiStats.stream().filter(wasPivot).collect(Collectors.toList());
        return filtered.subList(0, Math.min(topK, filtered.size()));
    }
}
