package com.kelvingu.relations.graph;

import edu.stanford.nlp.classify.GeneralDataset;
import edu.stanford.nlp.classify.RVFDataset;
import edu.stanford.nlp.ling.RVFDatum;
import edu.stanford.nlp.stats.ClassicCounter;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.util.CollectionFactory;
import edu.stanford.nlp.util.CollectionValuedMap;
import org.apache.tools.bzip2.CBZip2InputStream;

import java.io.*;
import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class Edge implements Serializable {

    public static final String FALSE_LABEL = "None";

    private static final long serialVersionUID = 6267987941970926114L;

    public final String source;
    public final String target;
    public final String relation;

    private long sourceId = -1;
    private long targetId = -1;
    private double score;
    private Boolean label;
    private Counter<String> features;
    private Map<String, String> attributes;
    private CollectionValuedMap<String, String> instances;

    public Edge(String source, String relation, String target) {
        this.source = source;
        this.target = target;
        this.relation = relation;
    }

    public boolean label() {
        if (label == null) {
            throw new NullPointerException("This edge is not labeled");
        }
        return label;
    }

    public Edge setLabel(boolean label) {
        this.label = label;
        return this;
    }

    public long sourceId() {
        if (sourceId == -1) {
            throw new NullPointerException("No source ID");
        }
        return sourceId;
    }

    public Edge setSourceId(long sourceId) {
        this.sourceId = sourceId;
        return this;
    }

    public long targetId() {
        if (targetId == -1) {
            throw new NullPointerException("No target ID");
        }
        return targetId;
    }

    public Edge setTargetId(long targetId) {
        this.targetId = targetId;
        return this;
    }

    public double score() {
        return score;
    }

    public Edge setScore(double score) {
        this.score = score;
        return this;
    }

    public Counter<String> features() {
        if (features == null) {
            features = new ClassicCounter<>();
        }
        return features;
    }

    public Edge setFeatures(Counter<String> features) {
        this.features = features;
        return this;
    }

    public Map<String, String> attributes() {
        return attributes;
    }

    public Edge setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
        return this;
    }

    public CollectionValuedMap<String, String> instances() {
        if (instances == null) {
            instances = new CollectionValuedMap<String, String>(CollectionFactory.ARRAY_LIST_FACTORY);
        }
        return instances;
    }

    @Override
    public String toString() {
        return String.format("%s | %s | %s", source, relation, target);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Edge)) {
            return false;
        }

        Edge rhs = (Edge) obj;
        return source.equals(rhs.source)
                && target.equals(rhs.target)
                && relation.equals(rhs.relation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, relation, target);
    }

    /**
     * A GeneralDataset is the format accepted by JavaNLP classifiers
     * @param edges
     * @return
     */
    public static GeneralDataset<String, String> toDataset(List<Edge> edges) {
        GeneralDataset<String,String> dataset = new RVFDataset<>();
        edges.forEach((e) -> {
            RVFDatum<String,String> datum = new RVFDatum<>(e.features(), e.label() ? e.relation : FALSE_LABEL);
            dataset.add(datum);
        });
        return dataset;
    }

    public static void toFile(List<? extends Edge> relationEdges, File dataFile) throws IOException {
        FileWriter fw = new FileWriter(dataFile);
        BufferedWriter bw = new BufferedWriter(fw);
        for (Edge re : relationEdges) {
            bw.write(re.toStringForFile());
            bw.newLine();
        }
        bw.close();
        fw.close();
    }

    // TODO: WARNING! Since the Edge class was updated, this method hasn't been updated to read the edge label from file
    public static Stream<Edge> lazyLoadFromFile(final File dataFile) {
        Iterable<Edge> it = new Iterable<Edge>() {
            @Override
            public Iterator<Edge> iterator() {
                final InputStream inputStream;
                try {
                    if (dataFile.getAbsolutePath().endsWith(".bz2")) {
                        InputStream inputStream_ = new FileInputStream(dataFile);
                        // Skip the first two 'magic' bytes because this 'library' thinks that's intuitive.
                        // Yeah, tell me about it.
                        inputStream_.read();
                        inputStream_.read();
                        inputStream = new CBZip2InputStream(inputStream_);
                    } else {
                        inputStream = new FileInputStream(dataFile);
                    }
                    final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    final String firstLine = reader.readLine();
                    return new Iterator<Edge>() {
                        final BufferedReader br = reader;
                        String lastLine = firstLine;

                        @Override
                        public void finalize() throws Throwable {
                            try {
                                br.close();
                                super.finalize();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public boolean hasNext() {
                            return lastLine != null;
                        }

                        @Override
                        public Edge next() {
                            String line = lastLine;

                            if (line == null) return null;

                            String[] tuple = line.split("\t");
                            String source = tuple[0];
                            String relation = tuple[1];
                            String target = tuple[2];

                            // load all features as key-value pairs
                            Map<String, String> attributes = new HashMap<>();
                            Counter<String> features = new ClassicCounter<>();
                            for (int i = 3; i < tuple.length; i++) {
                                String[] keyValue = tuple[i].split("=");
                                String key = keyValue[0];
                                if (keyValue.length == 1) {
                                    features.setCount(key, 1.);
                                } else {
                                    try {
                                        double value = Double.parseDouble(keyValue[1]);
                                        features.setCount(key, value);
                                    } catch (NumberFormatException e) {
                                        String value = keyValue[1];
                                        attributes.put(key, value);
                                    }
                                }
                            }

                            // score has its own special attribute, and is removed from features
                            // default value is 1
                            double score = 1;
                            if (features.containsKey("score")) {
                                score = features.getCount("score");
                                features.remove("score");
                            }

                            try {
                                lastLine = br.readLine();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }

                            return new Edge(source, relation, target)
                                    .setScore(score)
                                    .setFeatures(features)
                                    .setAttributes(attributes);
                        }

                        @Override
                        public void remove() {
                            throw new RuntimeException("invalid operation");
                        }
                    };
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };

        return StreamSupport.stream(it.spliterator(), false);
    }

    public static List<Edge> fromFile(File dataFile, int limit) throws IOException {
        List<Edge> edges = new ArrayList<>();

        InputStream inputStream;

        // standard input stream or BZip2 input stream
        if (dataFile.getAbsolutePath().endsWith(".bz2")) {
            inputStream = new FileInputStream(dataFile);
            // Skip the first two bytes because this 'library' is a joke.
            inputStream.read();
            inputStream.read();
            inputStream = new CBZip2InputStream(inputStream);
        } else {
            inputStream = new FileInputStream(dataFile);
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        while ((line = br.readLine()) != null) {
            String[] tuple = line.split("\t");
            String source = tuple[0];
            String relation = tuple[1];
            String target = tuple[2];

            // load all features as key-value pairs
            Map<String, String> attributes = new HashMap<>();
            Counter<String> features = new ClassicCounter<>();
            for (int i = 3; i < tuple.length; i++) {
                String[] keyValue = tuple[i].split("=");
                String key = keyValue[0];

                // if no value is specified for a feature, defaults to 1
                if (keyValue.length == 1) {
                    features.setCount(key, 1.);
                } else {
                    try {
                        double value = Double.parseDouble(keyValue[1]);
                        features.setCount(key, value);
                    } catch (NumberFormatException e) {
                        // if the value is not a number, it's an attribute string
                        String value = keyValue[1];
                        attributes.put(key, value);
                    }
                }
            }

            Edge newEdge = new Edge(source, relation, target)
                    .setFeatures(features)
                    .setAttributes(attributes);

            // score has its own special field, and is removed from features
            if (features.containsKey("score")) {
                newEdge.setScore(features.getCount("score"));
                features.remove("score");
            }

            // label has its own special field, and is removed from features
            if (attributes.containsKey("label")) {
                boolean label = Boolean.valueOf(attributes.get("label"));
                newEdge.setLabel(label);
                attributes.remove("label");
            }

            edges.add(newEdge);
            if (edges.size() == limit) break;
        }

        br.close();
        return edges;
    }

    public static List<Edge> fromFile(File dataFile) throws IOException {
        return fromFile(dataFile, -1);
    }

    public String toStringForFile() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s\t%s\t%s", source, relation, target));

        sb.append("\t").append(keyValuePair("score", score()));
        sb.append("\t").append(keyValuePair("label", label()));

        for (String feat : features().keySet()) {
            sb.append("\t").append(keyValuePair(feat, features().getCount(feat)));
        }
        return sb.toString();
    }

    private String keyValuePair(String key, Object value) {
        return String.format("%s=%s", key, value);
    }

}
