package com.kelvingu.relations.utils;

import java.io.*;

public class IO {

    public static void save(Object object, File f) throws IOException {
        // only works for objects that implement Serializable
        FileOutputStream fileOut = new FileOutputStream(f);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);

        out.writeObject(object);
        out.close();
        fileOut.close();
    }

    public static <T> T load(File f) throws IOException, ClassNotFoundException {
        // only works for objects that implement Serializable
        FileInputStream fileIn = new FileInputStream(f);
        ObjectInputStream in = new ObjectInputStream(fileIn);

        @SuppressWarnings("unchecked")
        T obj = (T) in.readObject();
        in.close();
        fileIn.close();
        return obj;
    }
}
