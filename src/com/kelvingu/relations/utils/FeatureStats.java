package com.kelvingu.relations.utils;

import com.kelvingu.relations.graph.Edge;
import edu.stanford.nlp.stats.Counter;
import edu.stanford.nlp.util.Pair;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class FeatureStats extends HashMap<String, FeatureStats.MultiStat> implements Serializable {

    // if true, any edge stored in FeatureStats will be a copy of the original observed edge
    // with its features stripped. The original edge will be untouched.
    public static boolean stripEdgeFeatures = true;

    private static final long serialVersionUID = -1865514236628383871L;

    // number of examples to store for a particular rule
    private static final int numExamples = Integer.MAX_VALUE;

    @Override
    public MultiStat get(Object feat) {
        if (!containsKey(feat)) {
            String featName = (String) feat;
            put(featName, new MultiStat(featName));
        }
        return super.get(feat);
    }

    public FeatureStats get(Set<String> features) {
        FeatureStats subFS = new FeatureStats();
        for (String feat : features) {
            subFS.put(feat, get(feat));
        }
        return subFS;
    }

    public FeatureStats get(Edge edge) {
        return get(edge.features().keySet());
    }

    public Map<String, FeatureStats> groupingByType() {
        Map<String, FeatureStats> grouping = new HashMap<>();
        for (String feat : keySet()) {
            String type = StringFeature.getType(feat);
            if (!grouping.containsKey(type)) {
                grouping.put(type, new FeatureStats());
            }
            FeatureStats fs = grouping.get(type);
            fs.put(feat, get(feat));
        }
        return grouping;
    }

    /**
     * retains only the top k features
     */
    public void retainTop(AttribSelector selector, int k) {
        List<Pair<String, Double>> pairs = keySet().stream()
                .map(feat -> new Pair<>(feat, selector.select(get(feat))))
                .collect(Collectors.toList());

        // note that this sorts in descending order
        Collections.sort(pairs, (p1, p2) -> -Double.compare(p1.second, p2.second));
        for (int i = k; i < pairs.size(); i++) {
            remove(pairs.get(i).first);
        }
    }

    /**
     * retains only features above the given quantile
     */
    public void retainAboveQuantile(AttribSelector selector, double quantile) {
        int total = keySet().size();
        int k = total - (int) Math.floor(total * quantile);
        retainTop(selector, k);
    }

    public void loadEdges(List<Edge> edges, String selection) {
        StatSelector selector;
        switch (selection) {
            case "train":
                selector = (ms) -> ms.train;
                break;
            case "test":
                selector = (ms) -> ms.test;
                break;
            case "train pivot":
                selector = (ms) -> ms.trainPivot;
                break;
            case "test pivot":
                selector = (ms) -> ms.testPivot;
                break;
            default:
                throw new IllegalArgumentException();
        }

        System.out.println("FeatureStats: collecting stats for " + selection);
        loadEdges(edges, selector);
    }

    public void loadEdges(List<Edge> edges, StatSelector selector) {
        int numPositive = 0;

        int total = edges.size();
        int count = 0;

        for (Edge edge : edges) {
            Counter<String> features = edge.features();
            int label = edge.label() ? 1 : 0;
            numPositive += label;

            for (String feat : features.keySet()) {
                Stat featStat = selector.select(get(feat));
                featStat.confusionMatrix[1][label]++;

                // keep track of positive and negative examples
                SampledList<Edge> examp = edge.label() ? featStat.labeledTrue : featStat.labeledFalse;

                Edge toBeAdded;
                if (stripEdgeFeatures) {
                    toBeAdded = new Edge(edge.source, edge.relation, edge.target).setLabel(edge.label());
                    toBeAdded.setFeatures(null);
                    toBeAdded.setAttributes(null);
                } else {
                    toBeAdded = edge;
                }

                examp.add(toBeAdded);
            }

            if (count % (total / 5) == 0) {
                System.out.println(count + " of " + total + " loaded");
            }
            count++;
        }

        int numNegative = edges.size() - numPositive;

        // fill in entries for when feature isn't present
        for (String feat : keySet()) {
            Stat featStat = selector.select(get(feat));
            int[][] cfm = featStat.confusionMatrix;
            cfm[0][0] = numNegative - cfm[1][0];
            cfm[0][1] = numPositive - cfm[1][1];
        }
    }

    public List<MultiStat> getStatsBy(AttribSelector selector, int topK) {
        List<MultiStat> features = new ArrayList<>(values());

        // -1 means take all features
        topK = topK == -1 ? features.size() : topK;

        // this sorts from largest to smallest
        Comparator<MultiStat> comparator = (o1, o2) -> Double.compare(selector.select(o2), selector.select(o1));
        Collections.sort(features, comparator);

        return features.subList(0, Math.min(topK, features.size()));
    }

    public List<MultiStat> getStatsByTrainProbExtremity(double pseudoCount, int topK) {
        AttribSelector selector = (ms) -> Math.abs(0.5 - ms.train.getProb(pseudoCount));
        return getStatsBy(selector, topK);
    }

    public List<MultiStat> getStatsByTestPivotNum(int topK) {
        AttribSelector selector = (ms) -> {
            int[][] cfm = ms.testPivot.confusionMatrix;
            return (double) cfm[1][0] + cfm[1][1];
        };
        return getStatsBy(selector, topK);
    }

    public List<MultiStat> getStatsByTestPivotProb(double pseudoCount, int topK) {
        return getStatsByTestPivotProb(pseudoCount, topK, true);
    }

    public List<MultiStat> getStatsByTestPivotProb(double pseudoCount, int topK, boolean descending) {
        int factor = descending ? 1 : -1;
        AttribSelector selector = ms -> factor * ms.testPivot.getProb(pseudoCount);
        return getStatsBy(selector, topK);
    }

    public List<MultiStat> getStatsByTestProb(double pseudoCount, int topK) {
        AttribSelector selector = (ms) -> ms.test.getProb(pseudoCount);
        return getStatsBy(selector, topK);
    }

    public List<MultiStat> getStatsByProbDiff(double trainPseudoCount, double testPseudoCount, int topK) {
        AttribSelector selector = twoStat -> {
            double trainProb = twoStat.train.getProb(trainPseudoCount);
            double testProb = twoStat.test.getProb(testPseudoCount);
            return Math.abs(trainProb - testProb);
        };
        return getStatsBy(selector, topK);
    }

    public static interface StatSelector {
        public Stat select(MultiStat multiStat);
    }

    public static interface AttribSelector {
        public double select(MultiStat multiStat);
    }

    public static class Stat implements Serializable {

        private static final long serialVersionUID = -6975658579679631057L;

        // these are a SAMPLE of the edges that have the feature. True label = true (pos) or false (neg)
        public final SampledList<Edge> labeledTrue;
        public final SampledList<Edge> labeledFalse;

        // confusion matrix maintains overall counts (not a sample)
        public int[][] confusionMatrix;

        public Stat(int numExamples) {
            labeledTrue = new SampledList<>(numExamples);
            labeledFalse = new SampledList<>(numExamples);
            confusionMatrix = new int[2][2];
        }

        public String toString() {
            return String.valueOf(getProb(0));
        }

        public double getProb(double pseudoCount) {
            double numPos = confusionMatrix[1][1] + pseudoCount;
            double numNeg = confusionMatrix[1][0] + pseudoCount;

            if (numNeg == 0 && numPos == 0) {
                return 0.5;
            }

            return numPos / (numPos + numNeg);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) { return false; }
            if (obj == this) { return true; }
            if (obj.getClass() != getClass()) {
                return false;
            }

            Stat rhs = (Stat) obj;
            return Arrays.deepEquals(confusionMatrix, rhs.confusionMatrix);
        }

        @Override
        public int hashCode() {
            return Arrays.deepHashCode(confusionMatrix);
        }

    }

    public static class MultiStat implements Serializable {
        private static final long serialVersionUID = 4030539538513457996L;
        public final String feature;
        public final Stat train;
        public final Stat test;
        public final Stat trainPivot;
        public final Stat testPivot;

        public MultiStat(String feature) {
            this.feature = feature;
            train = new Stat(numExamples);
            test = new Stat(numExamples);
            trainPivot = new Stat(numExamples);
            testPivot = new Stat(numExamples);

        }

        public String toString() {
            return feature;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) { return false; }
            if (obj == this) { return true; }
            if (obj.getClass() != getClass()) {
                return false;
            }

            MultiStat rhs = (MultiStat) obj;
            return Objects.equals(train, rhs.train)
                    && Objects.equals(test, rhs.test)
                    && Objects.equals(trainPivot, rhs.trainPivot)
                    && Objects.equals(testPivot, rhs.testPivot);
        }

        @Override
        public int hashCode() {
            return Objects.hash(train, test, trainPivot, testPivot);
        }

    }
}
