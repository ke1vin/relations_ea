package com.kelvingu.relations.utils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class SampledList<T> implements Iterable<T>, Serializable {

    private static final long serialVersionUID = 5520965098078930179L;
    private int capacity;
    private List<T> list;
    private transient Random rand;
    private int numSeen;

    public SampledList(int capacity) {
        this.capacity = capacity;
        list = new ArrayList<>();
        rand = new Random();
        numSeen = 0;
    }

    public boolean add(T t) {
        if (list.size() < capacity) {
            list.add(t);
        } else {
            int replaceIndex = rand.nextInt(numSeen+1);
            if (replaceIndex < capacity) {
                list.set(replaceIndex, t);
            }
        }
        numSeen++;
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(list.size());
    }

    public int size() {
        return list.size();
    }

    public T get(int index) {
        return list.get(index);
    }

    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        rand = new Random();
    }

    public List<T> getList() {
        return list;
    }

}
