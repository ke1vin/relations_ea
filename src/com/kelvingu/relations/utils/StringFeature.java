package com.kelvingu.relations.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Kelvin on 5/5/14.
 */
public class StringFeature {

    private static Pattern pattern = Pattern.compile("^\\s*:(\\S+):\\s+");

    /**
     * The type of a feature must be the first word of the String
     * and it must be enclosed by colons
     *
     * e.g. :path: *, gender, *, inv(gender), *
     *
     * If such an element is not found, this returns null.
     *
     */
    public static String getType(String feat) {
        Matcher matcher = pattern.matcher(feat);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    public static String addType(String feat, String type) {
        if (getType(feat) != null) {
            throw new IllegalArgumentException("The feature already has a type.");
        }

        return String.format(":%s: %s", type, feat);
    }

    public static String stripType(String feat) {
        if (getType(feat) == null) {
            return feat;
        }
        return feat.replaceFirst("^\\s*\\S+\\s+", "");
    }
}
